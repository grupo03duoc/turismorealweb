import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';
import {Usuario} from './usuario';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  create(user: Usuario): Observable<any> {
    return this.http.post<any>(`${AppSettings.URL}` + `/usuario/create`, user, httpOptions);
  }

  getUsuario(nombre:String): Observable<any> {
    return this.http.get(`${AppSettings.URL}/usuario/getUsuario/${nombre}`);
  }

}

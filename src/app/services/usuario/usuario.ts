export class Usuario{
    id: number;
    nombre:string;
    username:string;
    password:String;
    habilitado:number;
    correo:string;
    perfil:string;

    constructor(id:number,nombre: string, username: string, correo: string, password: string , perfil: string, habilitado:number)  {
        this.id = id;
        this.nombre = nombre;
        this.username = username;
        this.correo = correo;
        this.password = password;
        this.perfil = perfil;
        this.habilitado = habilitado;
    }
}
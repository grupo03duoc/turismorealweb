import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  constructor(private http: HttpClient) { }

  create(reserva:any): Observable<any> {
    return this.http.post<any>(`${AppSettings.URL}/reserva/create`,reserva,httpOptions);
  }

  getReservasList(id:number): Observable<any> {
    return this.http.get(`${AppSettings.URL}/reserva/getAllPorUsuario/${id}`);
  }

  getReserva(id:number):Observable<any> {
    return this.http.get(`${AppSettings.URL}/reserva/get/${id}`);
  }


}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TrasladoService {

  constructor(private http: HttpClient) { }

  create(post:any): Observable<any> {
    return this.http.post(`${AppSettings.URL}/traslado/create/`,post,httpOptions);
  }

}

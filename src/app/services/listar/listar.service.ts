import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ListarService {

  constructor(private http: HttpClient) { }

  getRegionesList(): Observable<any> {
    return this.http.get(`${AppSettings.URL}/lista/regiones`,httpOptions);
  }

  getComunasList(idProvincia:number): Observable<any> {
    return this.http.get(`${AppSettings.URL}/lista/comunas/${idProvincia}`);
  }

  getProvinciasList(idRegion:number): Observable<any> {
    return this.http.get(`${AppSettings.URL}/lista/provincias/${idRegion}`);
  }
}

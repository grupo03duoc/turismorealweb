import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {

  constructor(private http: HttpClient) { }

  getDepartamentosList(idComuna:number): Observable<any> {
    return this.http.get(`${AppSettings.URL}/departamento/all/${idComuna}`);
  }

  getDepartamento(id:number):Observable<any>{
    return this.http.get(`${AppSettings.URL}/departamento/get/${id}`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ToursService {

  constructor(private http: HttpClient) { }

  getTourList(id:number): Observable<any> {
    return this.http.get(`${AppSettings.URL}/tour/get_por_region/${id}`);
  }

  createReservaTour(post:any): Observable<any> {
    return this.http.post(`${AppSettings.URL}/tour/create/reservaTour`,post,httpOptions);
  }

}

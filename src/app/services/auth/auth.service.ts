import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtResponse } from './jwt-response';
import { Login } from './login';
import {AppSettings} from '../../server/AppSettings';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }


  attemptAuth(credentials: Login): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(`${AppSettings.URL}/auth/login`, credentials, httpOptions);
  }



}

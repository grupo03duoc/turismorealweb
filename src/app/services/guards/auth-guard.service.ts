import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TokenStorageService } from '../auth/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate  {

  constructor(private token: TokenStorageService, private router: Router) { }
  canActivate() {
    if (this.token.isAuthenticated()) {
      // login TRUE
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}

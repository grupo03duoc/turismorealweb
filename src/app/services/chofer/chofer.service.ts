import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AppSettings} from '../../server/AppSettings';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ChoferService {

  constructor(private http: HttpClient) { }

  getAllChofer(id:number):Observable<any> {
    return this.http.get(`${AppSettings.URL}/chofer/get_por_empresa/${id}`,httpOptions);
  }
}

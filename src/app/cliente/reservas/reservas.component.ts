import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { ReservaService} from '../../services/reserva/reserva.service';
import { TokenStorageService } from '../../services/auth/token-storage.service';
import { UserAuth } from '../../services/auth/userAuth';
import { UsuarioService } from '../../services/usuario/usuario.service';
@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.css']
})
export class ReservasComponent implements OnInit {
  displayedColumns: string[] = ['fechaInicio', 'fechaTermino', 'personas', 'total','pagar'];
  dataSource :any;
  root: UserAuth;
  conectado: any;
  user:any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private reservaService:ReservaService,private route:Router,private token: TokenStorageService,private usuarioService: UsuarioService) { }
  ngOnInit() {
    this.root = this.token.getUserAuth();
    if (this.root === undefined || this.root === null) {
      this.conectado = false;
    } else {
      this.conectado = true;
      this.usuarioService.getUsuario(this.root.username).subscribe(data => {
        this.user = data;
        this.getData();
      })
    }
   
  }

  getData(){

    this.reservaService.getReservasList(this.user.id).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    }

   );
  }

  pagar(obj:any){
    this.route.navigate(['/pago/'+obj.id+'/'+obj.departamento]);
  }
}


export interface Reserva {
  id:number,
  fechaInicio:string;
  fechaTermnio:string;
  personas: number;
  total:number;
  usuario: number;
  departamento:number;
}


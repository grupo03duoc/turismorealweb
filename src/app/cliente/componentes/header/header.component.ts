import { Component, OnInit } from '@angular/core';
import { UserAuth } from '../../../services/auth/userAuth';
import { TokenStorageService } from '../../../services/auth/token-storage.service';
import { Router } from "@angular/router";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  root: UserAuth;
  conectado:any;
  desconectado:any = false;
  constructor( private token: TokenStorageService,private router: Router) { }

  ngOnInit() {
    this.root = this.token.getUserAuth();
    if(this.root === undefined || this.root === null){
      this.conectado = false;
      this.desconectado = true;
    }else{
      this.conectado = true;
      this.desconectado = false;
    }
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }

  showMenu(){
    return true;
  }

  reservas(){
    if(this.conectado){
      this.router.navigate(['/reservas']);
    }else{
     Toast.fire({
       type: 'info',
       title: '¡Debe iniciar sesión!',
       text: 'No tiene permisos para ingresar al modulo'
     })
    }
  }

}
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

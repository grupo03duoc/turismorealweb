import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReservaService} from '../../services/reserva/reserva.service';
import {DepartamentoService} from '../../services/departamento/departamento.service';
import {PagoService} from '../../services/pago/pago.service';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})
export class PagoComponent implements OnInit {
  form: FormGroup;
  id:any;
  depa:any;
  departamento:any;
  reserva: any;
  tipoTarjeta =['Visa','MasterCard']
  meses = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  annos = ['2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030'];

  constructor(private formBuilder: FormBuilder,private router:Router,
    private route:ActivatedRoute,private reservaService:ReservaService, private departamentoService:DepartamentoService, private pagoService:PagoService,private datePipe: DatePipe) {
    this.form = this.formBuilder.group({
       tipoTarjeta:[this.tipoTarjeta[0],Validators.required],
       numeroTarjeta:['',[Validators.required,Validators.maxLength(16),Validators.minLength(16),Validators.pattern("^[0-9]*$")]],
       nombreTitular:['',Validators.required],
       cvc:['',[Validators.required,Validators.maxLength(3),Validators.minLength(3),Validators.pattern("^[0-9]*$")]],
       mes: [this.meses[0],Validators.required],
       anno: [this.annos[0],Validators.required],
       departamento: 0,
    });
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("reserva");
    this.depa =this.route.snapshot.paramMap.get("departamento");
    console.log(this.id);
    if(this.id !== null && this.depa !==null){
      this.getData();
   }
  }

  onSubmit() {
    
    var hoy= this.datePipe.transform(new Date(),"dd-MM-yyyy");
    var pago = {id:0,fecha:hoy,monto:this.reserva.total,tipoPago:2,reserva:this.reserva.id};
    this.pagoService.create(pago).subscribe(data =>{
      Swal.fire({
        type: 'success',
        title: 'Realizado',
        text: 'Pago Realizado Exitosamente',
        showConfirmButton: false,
        timer: 1500,
     });
      this.form.reset();
    })
  }

  getData(){

    this.reservaService.getReserva(this.id).subscribe(data => {
     this.reserva = data;
      this.departamentoService.getDepartamento(this.depa).subscribe(data => {
       this.departamento = data;
       });

    });
  }
}
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
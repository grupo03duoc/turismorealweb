import { Component, OnInit } from '@angular/core';
import {ListarService} from '../../services/listar/listar.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  comunas : Array<any>;
  regiones : Array<any>;
  provincias: Array<any>;
  idComuna: any;
  idRegion: any;
  idProvincia:any;
  disableSelect = true;
  disableSelectComuna = true;

  constructor(private listarService: ListarService,private route:Router) { }


  ngOnInit() {
    this.getRegiones();
  }

  getRegiones() {
    this.listarService.getRegionesList().subscribe(data => {
      this.regiones = data;
    });
  }

  getProvincias(id: any) {
    this.listarService.getProvinciasList(id).subscribe(data => {
      this.provincias = data;
      this.disableSelect = false;
    });
  }

  getComunas(id: any) {
    this.listarService.getComunasList(id).subscribe(data => {
      this.comunas = data;
      this.disableSelectComuna = false;

    });
  }

  buscar(){
     if(this.idComuna){
      this.route.navigate(['/departamentos/'+this.idComuna]);
     }else{
      Toast.fire({
        type: 'warning',
        title: '¡Complete el filtro!',
        text: 'No tiene una comuna Seleccionada'
      })
     }
     
  }

  public onChangeRegion(event): void {
    
    this.getProvincias(event.value);
  }

  public onChangeProvincia(event): void {
    this.getComunas(event.value);

    
  }

  public onChangeComuna(event): void {
    
  }

}
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DepartamentoService } from '../../services/departamento/departamento.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ErrorStateMatcher } from '@angular/material/core';
import Swal from 'sweetalert2';
import { ReservaService } from '../../services/reserva/reserva.service';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { TokenStorageService } from '../../services/auth/token-storage.service';
import { UserAuth } from '../../services/auth/userAuth';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { EmpresaService } from '../../services/empresa/empresa.service';
import { ChoferService } from '../../services/chofer/chofer.service';
import { ToursService } from '../../services/tours/tours.service';
import { TrasladoService } from '../../services/traslado/traslado.service';
@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
  id: any;
  departamento: any;
  total: any = 0;
  root: UserAuth;
  user:any;
  minDate = new Date();
  reservaForm: FormGroup;
  trasladoForm:FormGroup;
  fechaI: any = null;
  fechaT: any = null;
  disable: boolean = true;
  isChecked = false;
  isChecked2 = false;
  dias: any = 0;
  empresas: any;
  conectado: any;
  choferes:any;
  disableSelect = true;
  tours: any;
  totalTour: number = 0;
  tour:any;
  cantidad:number = 0;
  matcher = new MyErrorStateMatcher();
  constructor(
    private token: TokenStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private departamentoService: DepartamentoService,
    private reservaService: ReservaService,
    private usuarioService: UsuarioService,
    private empresaService: EmpresaService,
    private choferService:ChoferService,
    private toursService:ToursService,
    private trasladoService:TrasladoService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");
    if (this.id !== null) {
      this.getData();
    }
    this.root = this.token.getUserAuth();
    if (this.root === undefined || this.root === null) {
      this.conectado = false;
    } else {
      this.conectado = true;
      this.usuarioService.getUsuario(this.root.username).subscribe(data => {
        this.user = data;
      })
    }
    this.trasladoForm = new FormGroup({
      id: new FormControl(0),
      fecha: new FormControl('', [Validators.required]),
      ubicacion: new FormControl('', [Validators.required]),
      chofer: new FormControl(0, [Validators.required]),
      reserva: new FormControl(0),
    });

    this.reservaForm = new FormGroup({
      id: new FormControl(0),
      fechaInicio: new FormControl('', [Validators.required]),
      fechaTermino: new FormControl('', [Validators.required]),
      personas: new FormControl(0, [Validators.required]),
      usuario: new FormControl(0),
      departamento: new FormControl(this.id)
    });
  }

  onChange(event) {
    this.isChecked = event.checked;
  }
  onChangeTour(event){
    this.isChecked2 = event.checked;
    if(this.cantidad > 0 && this.isChecked2 && this.tour != null){
      this.totalTour = this.cantidad * this.tour.valor_persona;
      this.total = this.totalTour + (this.dias * this.departamento.valorNoche);
    }else if(!this.isChecked2){
      this.total = this.total - this.totalTour;
    }
  }
  select(event){
      
      this.cantidad = event.value;
      if(this.cantidad > 0 && this.isChecked2 && this.tour != null){
        this.totalTour = this.cantidad * this.tour.valor_persona;
        this.total = this.totalTour + (this.dias * this.departamento.valorNoche);
      }
  }
  onChangeChofer(event){
    this.chofer.setValue(event.value);
  }
  onChangeTourSelect(event){
     this.tour = event.value;
     if(this.cantidad > 0 && this.isChecked2 && this.tour != null){
      this.totalTour = this.cantidad * this.tour.valor_persona;
      this.total = this.totalTour + (this.dias * this.departamento.valorNoche);
    }
  }
  getData() {
    this.departamentoService.getDepartamento(this.id).subscribe(data => {
      this.departamento = data;
      this.toursService.getTourList(this.departamento.comuna.id).subscribe(data =>{
        this.tours = data;
      })
    });
    this.empresaService.getEmpresasList().subscribe(data =>{
      this.empresas = data;
    });

    
  }

  fechaInicioEvent(event: MatDatepickerInputEvent<Date>) {
    this.disable = false;
    this.fechaInicio.setValue(event.value);
    this.fechaI = event.value;
    if (this.fechaT != null) {
      var diasF = this.fechaT - this.fechaI;
      this.dias = diasF / (1000 * 60 * 60 * 24);
      this.total = this.totalTour + (this.dias * this.departamento.valorNoche);
    }
  }
  fechaTrasladoEvent(event: MatDatepickerInputEvent<Date>){
    this.fecha.setValue(event.value);
  }

  fechaTerminoEvent(event: MatDatepickerInputEvent<Date>) {
    this.fechaTermino.setValue(event.value);
    this.fechaT = event.value;
    var diasF = this.fechaT - this.fechaI;
    this.dias = diasF / (1000 * 60 * 60 * 24);
    this.total = this.totalTour + (this.dias * this.departamento.valorNoche);
  }

  onSubmit() {
    if (this.conectado) {
      const data = this.fechaInicio.value;
      const data2 = this.fechaTermino.value;
      var reserva: any = {
        id: 0,
        total: this.total,
        personas: this.personas.value,
        departamento: +this.id,
        fechaInicio: data.getDate() + '-' + (data.getMonth() + 1) + '-' + data.getFullYear(),
        fechaTermino: data2.getDate() + '-' + (data2.getMonth() + 1) + '-' + data2.getFullYear(),
        usuario: this.user.id
      }
     

      this.reservaService.create(reserva).subscribe(
        data => {
          var reserva = data;
          if(this.isChecked){
           const fechaTraslado = this.fecha.value;
           var traslado:any = {
            id:0,
            fecha:fechaTraslado.getDate() + '-' + (fechaTraslado.getMonth() + 1) + '-' + fechaTraslado.getFullYear(),
            ubicacion:this.ubicacion.value,
            chofer:this.chofer.value,
            reserva: reserva.id
          }
          
            this.trasladoService.create(traslado).subscribe();
          }
          if(this.isChecked2 && this.tour != null){
            var reservaTour: any ={
              reserva: reserva.id,
              tour:this.tour.id
            }
            this.toursService.createReservaTour(reservaTour).subscribe();
          }
          
          
          Swal.fire({
            type: 'success',
            title: 'Realizado',
            text: 'Reserva Realizada Exitosamente',
            showConfirmButton: false,
            timer: 1500,
          });
          this.reservaForm.reset();
          this.router.navigate(['home']);
        }
      ),
        error => {
          Swal.fire({
            type: 'error',
            title: 'Oops..',
            text: 'Error al realizar una Reserva',
            showConfirmButton: false,
            timer: 3000,
          });
        }
    } else {
      Toast.fire({
        type: 'info',
        title: '¡Debe iniciar sesión!',
        text: 'No tiene permisos para ingresar al modulo'
      })
    }

  }
  onChangeEmpresa(event){
      this.getChoferes(event.value);
  }

  getChoferes(id:any){
       this.choferService.getAllChofer(id).subscribe(data =>{
         this.choferes = data;
         this.disableSelect = false;
       })
  }
  get idReserva() { return this.reservaForm.get('id'); }
  get fechaInicio() { return this.reservaForm.get('fechaInicio'); }
  get fechaTermino() { return this.reservaForm.get('fechaTermino'); }
  get personas() { return this.reservaForm.get('personas'); }
  get usuario() { return this.reservaForm.get('usuario'); }
  get idDepa() { return this.reservaForm.get('departamento'); }

  get ubicacion() { return this.trasladoForm.get('ubicacion'); }
  get fecha() { return this.trasladoForm.get('fecha'); }
  get chofer() { return this.trasladoForm.get('chofer'); }
}
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import {UsuarioService} from '../../services/usuario/usuario.service';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  registroForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  hide = true;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.registroForm = new FormGroup({
      id : new FormControl(0),
      nombre: new FormControl('',[ Validators.required,Validators.minLength(3)]),
      username: new FormControl('', [Validators.required,Validators.minLength(3)]),
      correo: new FormControl( '',[Validators.required,Validators.email]),
      perfil:  new FormControl('Usuario'),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      habilitado:  new FormControl(1),
    });
  }
   // metodos para el html y las validaciones 
   get nombre() { return this.registroForm.get('nombre'); }
   get username() { return this.registroForm.get('username'); }
   get password() { return this.registroForm.get('password'); }
   get correo() { return this.registroForm.get('correo'); }

  onSubmit() {
    if (this.registroForm.invalid) {
      return;
    }
    this.usuarioService.create(this.registroForm.value)
    .subscribe(
      data =>{
       Swal.fire({
         type: 'success',
         title: 'Realizado',
         text: 'Usuario Agregado Exitosamente',
         showConfirmButton: false,
         timer:1500,
       });
       this.registroForm.reset();
      }
    ),
    error =>{
     Swal.fire({
       type: 'error',
       title: 'Oops..',
       text: 'Error al registrar al Usuario',
       showConfirmButton: false,
       timer:3000,
     });
    }

  }

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from "@angular/router";
import { AuthService } from '../../services/auth/auth.service';
import { TokenStorageService } from '../../services/auth/token-storage.service';
import { Login } from '../../services/auth/login';
import { JwtResponse } from '../../services/auth/jwt-response';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  jwtReponse: JwtResponse;
  loginForm: FormGroup;
  errorMessage = '';
  loading:any;
  private login: Login;
  hide = true;
  matcher = new MyErrorStateMatcher();
  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private router: Router) { }

  ngOnInit() {
    if (this.tokenStorage.isAuthenticated()) {
      this.router.navigate(['home']);
    }
    this.loading = true;
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }


  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }

  onSubmit() {
    this.loading = false;
    if (this.loginForm.invalid) {
      return;
    }

    this.login = new Login(
      this.username.value,
      this.password.value);

    this.authService.attemptAuth(this.login).subscribe(
      data => {
        this.loading = true;
        this.jwtReponse = data;
        window.sessionStorage.setItem('token', JSON.stringify(this.jwtReponse));
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);
        this.reloadPage();
      },
      error => {
        this.loading = true;
        console.log(error);
        if(error.status == 0){
          Toast.fire({
            type: 'warning',
            title: '¡Datos no Validos!',
            text: 'La contraseña o el username es incorrecto'
          })
        }  
      }
    );
  }


  reloadPage() {
      this.router.navigate(['/home']);
  }
}

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
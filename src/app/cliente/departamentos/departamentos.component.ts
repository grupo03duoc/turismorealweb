import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {DepartamentoService} from '../../services/departamento/departamento.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.css']
})
export class DepartamentosComponent implements OnInit {
  idComuna:any;
  departamentos:Array<any>;
  constructor(private router:Router,private route:ActivatedRoute, private departamentoService:DepartamentoService) { }

  ngOnInit() {
    this.idComuna = this.route.snapshot.paramMap.get("id");
      if(this.idComuna !== null){
         this.getData();
      }
   
  }

  getData(){
     this.departamentoService.getDepartamentosList(this.idComuna).subscribe(data => {
      this.departamentos = data;
    });
  }

  detalle(depa:any) {
    this.router.navigate(['/detalle/'+depa]);
  }

}

import { Injectable } from '@angular/core';
import { UserAuth } from './userAuth';
import { isNullOrUndefined } from "util";
import { JwtHelperService } from '@auth0/angular-jwt';

const TOKEN_KEY = 'AuthToken';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  private perfiles: Array<string> = [];
  constructor(public jwtHelper: JwtHelperService) { }

  signOut() {
    window.sessionStorage.clear();
  }

  public isAuthenticated(): boolean {
    const token = window.sessionStorage.getItem(TOKEN_KEY);
    // revisa si el token esta vacido retorna
    // true o false
    return !this.jwtHelper.isTokenExpired(token);
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUsername(username: string) {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY, username);
  }

  public getUsername(): string {
    return sessionStorage.getItem(USERNAME_KEY);
  }

  public saveAuthorities(authorities: string[]) {
    window.sessionStorage.removeItem(AUTHORITIES_KEY);
    window.sessionStorage.setItem(AUTHORITIES_KEY, JSON.stringify(authorities));
  }

  public getAuthorities(): string[] {
    this.perfiles = [];

    if (sessionStorage.getItem(TOKEN_KEY)) {
      JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY)).forEach(authority => {
        this.perfiles.push(authority.authority);
      });
    }

    return this.perfiles;
  }

  public getUserAuth():UserAuth{
    if (!isNullOrUndefined(this.getToken())) {
    var rol =this.getAuthorities();
    var perfil;
    var username = this.getUsername();
     rol.forEach(val =>{
       switch (val) {
         case "ROLE_USER":
           perfil = 'Usuario';
           break;
         case "ROLE_SUPERUSER":
            perfil = 'Usuario Avanzado';
           break;
          case "ROLE_ADMIN":
            perfil= 'Administrador';
     
         break;
         } 
     })
    let user:UserAuth = {
      nombre:null,
      username: username,
      perfil: perfil,
    } 
    return user;
  }else {
    return null;
  }
    
  }
}

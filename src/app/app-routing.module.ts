import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './cliente/home/home.component';
import {LoginComponent} from './cliente/login/login.component';
import {RegistroComponent} from './cliente/registro/registro.component';
import {PagoComponent} from './cliente/pago/pago.component';
import {DetalleComponent} from './cliente/detalle/detalle.component';
import {DepartamentosComponent} from './cliente/departamentos/departamentos.component';
import {ReservasComponent} from './cliente/reservas/reservas.component';
import {AuthGuardService as AuthGuard } from '../app/services/guards/auth-guard.service';
const routes: Routes = [
  {path: '', redirectTo: 'home',pathMatch: 'full' },
  {path:'home', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'pago/:reserva/:departamento', component:PagoComponent},
  {path:'registro', component:RegistroComponent},
  {path:'detalle/:id', component:DetalleComponent},
  {path:'departamentos/:id', component:DepartamentosComponent},
  {path:'reservas', component:ReservasComponent,canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

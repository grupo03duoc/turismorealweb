import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {MatDatepickerModule,MatNativeDateModule, MatFormFieldModule,MatInputModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './cliente/login/login.component';
import { RegistroComponent } from './cliente/registro/registro.component';
import { HomeComponent } from './cliente/home/home.component';
import { DetalleComponent } from './cliente/detalle/detalle.component';
import { PagoComponent } from './cliente/pago/pago.component';
import { FooterComponent } from './cliente/componentes/footer/footer.component';
import { HeaderComponent } from './cliente/componentes/header/header.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FeaturesComponent } from './cliente/componentes/features/features.component';
import { DepartamentosComponent } from './cliente/departamentos/departamentos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common'; 
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material';
import { ReservasComponent } from './cliente/reservas/reservas.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatExpansionModule} from '@angular/material/expansion';
import {DatePipe} from '@angular/common';
export function jwtTokenGetter() {
  return sessionStorage.getItem('AuthToken');
}
const JWT_Module_Options: JwtModuleOptions = {
  config: {
      tokenGetter: jwtTokenGetter 
  }
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    HomeComponent,
    DetalleComponent,
    PagoComponent,
    FooterComponent,
    HeaderComponent,
    FeaturesComponent,
    DepartamentosComponent,
    ReservasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatButtonModule,
    MatSlideToggleModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    JwtModule.forRoot(JWT_Module_Options)
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
